# nth-mo

Gestión del modelo operativo para SEMPERTI. Se puede editar este modelo de 2 maneras:
- usando la GUI de Gitlab a través de https://gitlab.com/diego.semperti/modelo-operativo/-/tree/master
- siguiendo los requisitos debajo e instalando localmente. Esto se recomienda para usuarios avanzado de Git.

## Requisitos
1. Instalar:
- git
- python/pip (prueba)
- mkdocs (https://www.mkdocs.org/)
- Mkdocs Material Theme 

2. Crear usuario en Gitlab y solicitar permiso de developer en el proyecto (se gestiona desde un grupo de miembros).

3. Pasos a seguir para comenzar a editar:
- clonar proyecto de Gitlab: git clone https://gitlab.com/diego.semperti/modelo-operativo.git
- pip install mkdocs
- pip install mkdocs-material
- mkdocs build
- mkdocs serve e ingresar a http://127.0.0.1:8000
