### Comunicación

Nuestra herramienta de comunicación corporativa es el SLACK.
Allí tenemos muchos canales donde conversamos e informamos sobre temas varios.
Te pedimos por favor que te lo descargues y actives las notificaciones para que nos podamos mantener en contacto !
 
- [Slack](http://slack.com)

