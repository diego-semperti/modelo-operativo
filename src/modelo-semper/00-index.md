

El Modelo Operativo define la manera en que las personas colaboran para lograr los objetivos descriptos en la estrategia de la empresa.
Son las personas, los procesos y la tecnología que hacen que las cosas sucedan y que los objetivos se alcancen.

En este sitio encontrarás toda la informacion necesaria para que puedas entender como trabajamos, hacia donde vamos, cuales son nuestras metas y como pensamos lograrlas.



