

#Procesos de Comercial#

Comercial es nuestra area de negocios para crecer en nuevos clientes. 
Es el punto donde nace un nuevo cliente. Es desde donde trabajamos nuestro crecimiento genuino como empresa.

Comercial tiene 2 verticales de procesos:


![Ver Mapa de Procesos](./../../assets2/mapa-comercial.png)

### Enlace a procesos de Comercial 

- [Oportunidades](https://docs.google.com/document/d/1unj-5vr3JfUbcpijNQyChYh6IKhPoLPi842QrD_xAFo/edit?usp=sharing)
- [Propuestas](https://docs.google.com/document/d/1lMHT1togKq4LndMv-yiUdyzBPHDRTUb1NuCOIxlbw00/edit?usp=sharing)
- [P&L](https://docs.google.com/document/d/1hfdFoX_VyhcSe4uVnk6CPGKssADHFVUtVvxcUGGaqjU/edit?usp=sharing)
- [Ordenes de compra](https://docs.google.com/document/d/1mGjxb7I3ay-EK1j4B-_my3VLVeVxb8nBETm_hyZpfBM/edit?usp=sharing)
- [Cierres](https://docs.google.com/document/d/1P4MvFDpYK4FdxcSfpbUtvfgphmOUW-ED_etsAIE4yII/edit?usp=sharing)