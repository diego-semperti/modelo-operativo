

#Mapa de Procesos#

Este es nuetro mapa de procesos. En el se pueden observar todos los procesos que se llevan a cabo durante el ciclo de vida de un negocio.
Los procesos se encuentran divididos por areas y, algunos de ellos, tienen dependencias temporales con otros.
Vas a poder entender en detalle que es lo que hacemos en cada caso, explorando la siguiente seccion donde los explicamos en detalle.

![Ver Mapa de Procesos](./../../assets2/mapa-procesos.png)

[Ampliar](./../../assets2/mapa-procesos.png)