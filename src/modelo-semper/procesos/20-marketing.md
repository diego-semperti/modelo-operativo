

#Procesos de Marketing#

Marketing es nuestro primer punto de contacto con los clientes. 
Es el punto donde nace un "Lead", termino que se refiere a un cliente potencial. Es desde donde trabajamos nuestra imagen de marca, nuestras alianzas y certificaciones y desde donde producimos la "generacion de demanda" que podra luego generar nuevos negocios.

Marketing tiene 3 verticales de procesos:


![Ver Mapa de Procesos](./../../assets2/mapa-marketing.png)

### Enlace a procesos de Marketing 

- [Alianzas, Acreditaciones y Certificaciones](https://docs.google.com/document/d/1Cu03vu0vTyBl7WCGO2QTFZWmGxM09Gl2fp-0h360dgs/edit?usp=sharing)
- [Proveedores](https://docs.google.com/document/d/1FZX1npYaAGHzjyl9eGluQ0pLfkqP9dRYJWUOpnYJmJw/edit?usp=sharing)
- [Reembolsos](https://docs.google.com/document/d/1DeaJBDBQ-bU6p9vX5aoT2PAnlkeu7XJuGFD6hzQ7xcE/edit?usp=sharing)
- [Compras](https://docs.google.com/document/d/19ibuCm1PshzxXJLV553-vbktxRkteQKVNswv9s3Ak8I/edit?usp=sharing)
- [Generacion de Leads](https://docs.google.com/document/d/1aLYu-3Z0xJwFUoHOE7teed2iBsFv16gF0o-ILllZD-Y/edit?usp=sharing)
- [Demanda](https://docs.google.com/document/d/1CjvZszhOn6UGr5Evhz-UhiFWP4R9XVMk9HOIIx1MFAs/edit?usp=sharing)