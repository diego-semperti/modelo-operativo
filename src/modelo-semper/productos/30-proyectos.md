### Definición

Un Proyecto es un conjunto de actividades operativas que compromenten la contrucción y entrega de un determinado producto, en un determinado y bajo determinadas condiciones.
El proyecto finaliza al concluir con los compromisos de entrega que han sido descritos en las especificaciones de su propuesta técnica.
Un ejemplo de ellos puede ser la instalación de una determinada plataforma, o el desarrollo de un software de integración.

