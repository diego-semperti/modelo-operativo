### Definición

Un Producto es una caja cerrada que se le vende a un cliente y que contiene elementos o servicios gestionados por una tercera parte.
Ejemplo de ellos pueden ser, la venta de hardware o la venta de un paquete de servicios operado por un tercero, bajo las condiciones cerradas que impone el mismo, y sobre el que SEMPERTI no tiene injerencia operativa alguna.

