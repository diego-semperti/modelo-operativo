### Definición

Llamamos Servicios Profesionales a aquellos en los cuales entregamos horas de un especialista para que el cliente lo gestiona a su voluntad.
En este tipo de servicios, solo tenemos la responsabilidad de proveer las horas del personal involucrado, no tenemos control sobre sus objetivos ni sobre sus actividades, las cuales son responsabilidad absoluta del cliente.
Dada la imposibilidad de dar valor agregado mas que la provisión de personal, SEMPERTI evita este tipo de servicios al cliente.
