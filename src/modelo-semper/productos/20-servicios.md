### Definición

Un Servicio es algo que genera una actividad de entrega repetitiva en el tiempo.
Tiene un principio y un final solo dados por una condición contractual, no depende las entregas realizadas durante el mismo.
Un ejemplo de ellos puede ser, un servicio de soporte y evolución de una determinada plataforma.