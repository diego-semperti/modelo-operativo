Los nuevos colaboradores completan un proceso de onboarding junto con un programa de entrenamiento específico para cada rol. 
También hay un conjunto de charlas que cada nuevo colaborador recibe sobre temas que son parte del ADN de SEMPERTI y sus operaciones.

#### Alta del nuevo colaborador
El proceso de alta para los nuevos colaboradores comienza con un primer contacto de parte del equipo de recruiting de SEMPERTI.
Posteriormente, se procede con los siguientes pasos: 

- Envío de formulario de Alta temprana y propuesta. Aquí se solicitará la información personal del colaborador necesaria para realizar el alta, junto con el detalle de la propuesta donde especificarán las condiciones pactadas.
- Envío de material de trabajo y Welcome Kit SEMPERTI.



#### Recepción de nuevo colaborador
Como parte de la bienvenida a nuevos integrantes relizamos las siguientes actividades durante las primeras 2 semanas posteriores al ingreso:

- Charla introductoria a SEMPERTI.
- Entrevistas con responsables de áreas.
- Generación de accesos.


#### Charla introductoria a SEMPERTI  
Responsable de coordinar: RRHH.  
Duración: 1 hora.  
Cuándo sucede: primer día hábil desde su ingreso.  
Objetivo: introducir al nuevo colaborador a la empresa, repasar la historia de SEMPERTI, revisar documentación del nuevo colaborador, firmar papeles para legajo, comentar recomendaciones de comunicación y temas generales de buenas prácticas y sugerencias para el trabajo remoto y presencial.  


#### Entrevistas con responsables de áreas  
Responsable de coordinar: RRHH.  
Duración: primeras 2 semanas.  
Cuándo sucede: según disponibilidad de agenda de los involucrados.  
Objetivo: que el nuevo colaborador entienda el trabajo de las diferentes áreas de SEMPERTI y cómo interactúan entre ellas.  


  
#### Generación de accesos  
Responsable de coordinar: Líder directo.  
Duración: durante las primeras 2 semanas en SEMPERTI.  
Cuándo sucede: según disponibilidad de agenda.  
Objetivo: que el nuevo colaborador tenga acceso a las diferentes herramientas necesarias para su trabajo diario.  

