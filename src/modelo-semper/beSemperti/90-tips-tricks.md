En esta sección se podrán visualizar algunos ejemplos para dar formato al contenido que agreguemos en estos documentos.

    
    !!! Ejemplos
    Para crear encabezados hay que usar el caracter especial #. Los niveles van desde h1 hasta h6.
    Para crear cada uno de ellos basta con anteponer a una palabra o frase la cantidad de niveles 
    en el cuál deseamos el encabezado.


    
### The 3rd level    |  #### The 4th level    | ##### The 5th level 
    
### The 3rd level

#### The 4th level

##### The 5th level

###### The 6th level

---

**Para crear negritas**

se deben colocar 2 * al comienzo y al final de la palabra o frase a formatear

*Para cursiva*

se debe colocar 1 * al comienzo y al final de la palabra o frase a formater

> agregar cita textual

se debe colocar un signo mayor al comienzo de la cita (>)

---

**Imágenes**

<div style="width:50%; margin:0 auto">
   <img src="https://octodex.github.com/images/minion.png" alt="image with reference link">
</div>


Entre  [] se debe agregar el texto que queremos poner como label del link y a continuación entre () la ruta.
