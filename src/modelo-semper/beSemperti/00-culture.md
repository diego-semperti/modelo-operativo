### Introducción


## Somos especialistas en Servicios Cloud e Innovación Digital


En **SEMPERTI** nos dedicamos a brindar servicios digitales para satisfacer las necesidades de nuestros clientes buscando aportar valor a su negocio. 
Valoramos ampliamente el desarrollo personal y profesional de nuestros colaboradores, como así también el trabajo en equipo, con el objetivo de lograr nuestras metas de calidad y crecimiento.


- [Enfoque en el cliente](#enfoque-en-el-cliente)
- [Innovacion](#innovacion)
- [Excelencia](#excelencia)
- [Trabajo en equipo](#trabajo-en-equipo)
- [Honestidad](#honestidad)



### Enfoque en el cliente

Hacemos que nuestros clientes formen parte de los equipos de trabajo, buscamos mantener compromiso conjunto que ayude a encontrar el éxito en nuestros servicios.
Entendemos que una venta solo es exitosa cuando un cliente nos vuelve a comprar.


### Innovacion


Dedicamos un espacio para investigar y encontrar ideas para innovar y mejorar nuestros servicios. 



### Excelencia


Nos enfocamos en los procesos iterativos, armamos células de trabajo multidisciplinarias y autónomas guiadas por una retroalimentación frecuente de nuestros clientes. 


### Trabajo en equipo


Somos una empresa de servicios por lo cual, la calidad profesional y humana de nuestros colaboradores es lo que nos diferencia del resto. 
Fomentamos un ambiente de trabajo amigable y una cultura de trabajo en equipo, compartiendo ideas y opiniones basados en una estructura horizontal.


### Honestidad


Reforzamos los principios de transparencia y colaboración. 
Tratamos de demostrar siempre honestidad en nuestras decisiones y acciones, tanto con nuestros colaboradores como con nuestros clientes.


