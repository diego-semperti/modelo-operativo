
![Natural](src/assets/nth_logo.png)


El Modelo Operativo persigue como finalidad narrar y describir las diferentes etapas, prácticas y procesos de Natural Tech House. 

Dicho modelo debe servir como guía y material de consulta para lograr una manera de trabajo estandarizada y sustentable a lo largo de la vida de los proyectos que desempeñemos.

Es un documento vivo que debe reflejar las mejores prácticas y procesos de la organización en un determinado momento de tiempo. Sus actualizaciones frecuentes y mejoras resultarán en su mayoría de las dinámicas y feedback que pueda aportar cada miembro de la organización.

El Modelo Operativo cuenta con las siguientes secciones:

- Introducción: Presenta cómo se organizan los equipos de Natural Tech House para llevar adelante los proyectos.
- Modelo de Referencia: Narración y descripción conceptual de prácticas cuyo fin es ayudar a entender las principales definiciones y proveer un marco teórico.
- Modelo Instanciado: Instanciación de cómo las herramientas, los procesos y los roles se utilizarán en Natural. La información en esta sección deriva del modelo de referencia y explica cómo se aplica a la empresa.
- Herramientas: Detalle del tooling que se utilizará para desplegar el modelo operativo.
- Templates: Conjunto de plantillas para poder desplegar el modelo operativo y así también entregables que surjen del mismo.
- Be Natural: 

